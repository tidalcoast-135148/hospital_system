/*
 Navicat Premium Data Transfer

 Source Server         : db
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : 127.0.0.1:3306
 Source Schema         : hospital_system

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 09/04/2022 15:08:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL,
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of persistent_logins
-- ----------------------------
INSERT INTO `persistent_logins` VALUES ('admin', 'e9L5twknpRKNfjq7rL0prw==', 'dRONCCPeN4HtGC+AXD3zqw==', '2021-08-15 16:46:00');

-- ----------------------------
-- Table structure for sys_backend_api_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_backend_api_table`;
CREATE TABLE `sys_backend_api_table`  (
  `backend_api_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '主键',
  `backend_api_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'API名称',
  `backend_api_url` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'API请求地址',
  `backend_api_method` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'API请求方式：GET、POST、PUT、DELETE',
  `pid` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父ID',
  `backend_api_sort` int(0) NOT NULL COMMENT '排序',
  `description` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`backend_api_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_backend_api_table
-- ----------------------------
INSERT INTO `sys_backend_api_table` VALUES ('030f481c4f99d46ef79f46274e475cd3', '挂号管理', 'none', 'POST,GET', NULL, 400, '挂号管理');
INSERT INTO `sys_backend_api_table` VALUES ('1', '所有接口', '/**', 'GET,POST,PUT,DELETE', NULL, 100, NULL);
INSERT INTO `sys_backend_api_table` VALUES ('10c6c48741e476581f0d3042a3f7d603', '插入门诊信息', '/clinic/insertOutPatient', 'POST,GET', '6d43e65ed830983304ea54ad4d8bfacc', 303, '插入门诊信息');
INSERT INTO `sys_backend_api_table` VALUES ('6d43e65ed830983304ea54ad4d8bfacc', '门诊接口', 'none', 'POST,GET', NULL, 300, '门诊管理相关功能');
INSERT INTO `sys_backend_api_table` VALUES ('744f401f5a9d9ecf93ff2b3ac29ae4d1', '体检管理', 'none', 'POST,GET', NULL, 500, '体检管理');
INSERT INTO `sys_backend_api_table` VALUES ('809cb6fb8aa5eb58b8885bd876976007', '查询体检信息', '/selectByCardId', 'POST,GET', '744f401f5a9d9ecf93ff2b3ac29ae4d1', 501, '根据卡号查询体检信息');
INSERT INTO `sys_backend_api_table` VALUES ('86c540ea132d1954695b089257650b2a', '查询门诊表', '/clinic/selectOutPatient', 'POST,GET', '6d43e65ed830983304ea54ad4d8bfacc', 301, '根据卡号查询门诊表');
INSERT INTO `sys_backend_api_table` VALUES ('a2f45a0b1b66e5d9043b0f98ff356c9c', '查找', '/search', 'GET', 'f5669a62ff1c8f20dc97594dffe05f5e', 201, NULL);
INSERT INTO `sys_backend_api_table` VALUES ('a673f33857b3b65268bba3a1e71cd582', '插入处方号', '/clinic/generatePrescriptionNum', 'GET,POST', '6d43e65ed830983304ea54ad4d8bfacc', 302, '插入处方号');
INSERT INTO `sys_backend_api_table` VALUES ('e4348b7ab7713916e0accb52db8cd441', '给病人添加个人史，家族史', '/clinic/updatePatient', 'POST,GET', '6d43e65ed830983304ea54ad4d8bfacc', 304, '给病人添加个人史，家族史');
INSERT INTO `sys_backend_api_table` VALUES ('ea9c194a200fb3fd9ff292eb3434e5a9', '查找病人', '/register/selectPatient', 'POST,GET', '030f481c4f99d46ef79f46274e475cd3', 401, '查找病人');
INSERT INTO `sys_backend_api_table` VALUES ('f5669a62ff1c8f20dc97594dffe05f5e', '用户接口', '/user', 'GET,POST,PUT,DELETE,OPTIONS', NULL, 200, NULL);

-- ----------------------------
-- Table structure for sys_frontend_menu_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_frontend_menu_table`;
CREATE TABLE `sys_frontend_menu_table`  (
  `frontend_menu_id` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '主键',
  `frontend_menu_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '前端菜单名称',
  `frontend_menu_url` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '前端菜单访问地址',
  `pid` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父ID',
  `frontend_menu_sort` int(0) NOT NULL COMMENT '排序',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`frontend_menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_frontend_menu_table
-- ----------------------------
INSERT INTO `sys_frontend_menu_table` VALUES ('082a4ae846138f375f7ac0d4368f5f6a', '用户管理', '/user', '', 200, 'userCenter', 'User Center');
INSERT INTO `sys_frontend_menu_table` VALUES ('13f84b1266f9a5ba00c751b2278a9d87', '门诊收费', '/outpatientCharges', '3ba716cafd033b6455b50db61515cf01', 301, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('1bede0a58d1e7a205a1ddd0f0e2344a0', 'API管理', '/api', '460c9d1ea88819a5173cb93c8b6ae5b9', 102, NULL, 'Api Management	');
INSERT INTO `sys_frontend_menu_table` VALUES ('316a871b74401eef579073f559cd2add', '角色管理', '/role', '082a4ae846138f375f7ac0d4368f5f6a', 202, NULL, 'Role Management');
INSERT INTO `sys_frontend_menu_table` VALUES ('32870cd6495e266bf5dae9b667f92868', 'api分配', '/roleapi', '460c9d1ea88819a5173cb93c8b6ae5b9', 207, NULL, NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('3ba716cafd033b6455b50db61515cf01', '划价收费', '/charge', '', 300, 'toll', 'Toll Management');
INSERT INTO `sys_frontend_menu_table` VALUES ('3bbde7e74e4d5db63e17cd6a84b4bd07', '普通门诊', '/clinic', 'bd623651825b5111be1b15105618dfb4', 601, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('460c9d1ea88819a5173cb93c8b6ae5b9', '系统管理', '/manage', '', 100, 'sys', 'System Set');
INSERT INTO `sys_frontend_menu_table` VALUES ('60ecb6f36d0c564cf529bd3ab690868a', '挂号记录', '/record', 'fb43f13e9bc0b8f39e1a49b6d532ecb5', 701, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('62d4bf7a955f02b18cd6c40a89f980fb', '菜单管理', '/menu', '460c9d1ea88819a5173cb93c8b6ae5b9', 101, NULL, 'Menu Management	');
INSERT INTO `sys_frontend_menu_table` VALUES ('775571c7a7312267c2f8c5e90106710e', '角色分配', '/roleuser', '460c9d1ea88819a5173cb93c8b6ae5b9', 204, NULL, 'Role User');
INSERT INTO `sys_frontend_menu_table` VALUES ('797b2e7d4f27ac9b20ecfd1becd41443', '菜单分配', '/rolemenu', '460c9d1ea88819a5173cb93c8b6ae5b9', 206, NULL, 'Menu Role');
INSERT INTO `sys_frontend_menu_table` VALUES ('7b770427afe6ac5d93c79751660f6047', '药库管理', '/drugs', '', 400, 'drugStore', 'Drug Administration');
INSERT INTO `sys_frontend_menu_table` VALUES ('8b3d9cff2d59532f8034802c594113f8', '药房管理', '/takeDrug', '', 500, 'takingDrug', 'Take Drug');
INSERT INTO `sys_frontend_menu_table` VALUES ('9af320c3c8653f1b4365fb46ed8009be', '挂号收费', '/charge', 'fb43f13e9bc0b8f39e1a49b6d532ecb5', 702, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('a153ff5458f214762c758e28277527c8', '普通体检', '/examination', 'ff3a02a29bf129fece80501c1cb4c999', 801, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('b3dfd292f4a398c7a502ccba02aa1abe', '体检收费', '/physicalExamination', '3ba716cafd033b6455b50db61515cf01', 302, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('bd623651825b5111be1b15105618dfb4', '门诊管理', '/clinic', '', 600, 'treatment', 'Treatment Management');
INSERT INTO `sys_frontend_menu_table` VALUES ('be73e07969d1a6d3c0891f96c43541d3', '药房取药', '/takingDrug', '8b3d9cff2d59532f8034802c594113f8', 501, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('c987bc7573165acdb4d3c1ae1473915c', '用户信息管理', '/userInfo', '082a4ae846138f375f7ac0d4368f5f6a', 203, NULL, 'User Management');
INSERT INTO `sys_frontend_menu_table` VALUES ('d0921711f854419f3835994eb706ac96', '出入库管理', '/storageIn', '7b770427afe6ac5d93c79751660f6047', 401, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('facf38577c03dca949bc7979106976f7', '药品管理', '/drugAdministration', '7b770427afe6ac5d93c79751660f6047', 402, '', NULL);
INSERT INTO `sys_frontend_menu_table` VALUES ('fb43f13e9bc0b8f39e1a49b6d532ecb5', '挂号管理', '/register', '', 700, 'register', 'Register Management');
INSERT INTO `sys_frontend_menu_table` VALUES ('ff3a02a29bf129fece80501c1cb4c999', '体检管理', '/examination', '', 800, 'technology', 'Examination Management');

-- ----------------------------
-- Table structure for sys_role_backend_api_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_backend_api_table`;
CREATE TABLE `sys_role_backend_api_table`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `backend_api_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'API管理表ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_backend_api_table
-- ----------------------------
INSERT INTO `sys_role_backend_api_table` VALUES (1, '1', '1');
INSERT INTO `sys_role_backend_api_table` VALUES (30, 'a1abb41df89147cb72ef19fc977ce261', 'f5669a62ff1c8f20dc97594dffe05f5e');
INSERT INTO `sys_role_backend_api_table` VALUES (40, '1561a0e487e753eaf59ea903304b0147', '744f401f5a9d9ecf93ff2b3ac29ae4d1');
INSERT INTO `sys_role_backend_api_table` VALUES (41, '1561a0e487e753eaf59ea903304b0147', 'ea9c194a200fb3fd9ff292eb3434e5a9');
INSERT INTO `sys_role_backend_api_table` VALUES (42, '1561a0e487e753eaf59ea903304b0147', '86c540ea132d1954695b089257650b2a');
INSERT INTO `sys_role_backend_api_table` VALUES (43, '1561a0e487e753eaf59ea903304b0147', '10c6c48741e476581f0d3042a3f7d603');
INSERT INTO `sys_role_backend_api_table` VALUES (44, '1561a0e487e753eaf59ea903304b0147', 'a673f33857b3b65268bba3a1e71cd582');
INSERT INTO `sys_role_backend_api_table` VALUES (45, '1561a0e487e753eaf59ea903304b0147', '6d43e65ed830983304ea54ad4d8bfacc');
INSERT INTO `sys_role_backend_api_table` VALUES (46, '1561a0e487e753eaf59ea903304b0147', '809cb6fb8aa5eb58b8885bd876976007');
INSERT INTO `sys_role_backend_api_table` VALUES (47, '1561a0e487e753eaf59ea903304b0147', '030f481c4f99d46ef79f46274e475cd3');
INSERT INTO `sys_role_backend_api_table` VALUES (48, '1561a0e487e753eaf59ea903304b0147', 'e4348b7ab7713916e0accb52db8cd441');

-- ----------------------------
-- Table structure for sys_role_frontend_menu_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_frontend_menu_table`;
CREATE TABLE `sys_role_frontend_menu_table`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `frontend_menu_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '前端菜单管理ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 225 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_frontend_menu_table
-- ----------------------------
INSERT INTO `sys_role_frontend_menu_table` VALUES (145, 'a1abb41df89147cb72ef19fc977ce261', '082a4ae846138f375f7ac0d4368f5f6a');
INSERT INTO `sys_role_frontend_menu_table` VALUES (146, 'a1abb41df89147cb72ef19fc977ce261', '460c9d1ea88819a5173cb93c8b6ae5b9');
INSERT INTO `sys_role_frontend_menu_table` VALUES (195, '1', '9af320c3c8653f1b4365fb46ed8009be');
INSERT INTO `sys_role_frontend_menu_table` VALUES (196, '1', '1bede0a58d1e7a205a1ddd0f0e2344a0');
INSERT INTO `sys_role_frontend_menu_table` VALUES (197, '1', '460c9d1ea88819a5173cb93c8b6ae5b9');
INSERT INTO `sys_role_frontend_menu_table` VALUES (198, '1', 'c987bc7573165acdb4d3c1ae1473915c');
INSERT INTO `sys_role_frontend_menu_table` VALUES (199, '1', '7b770427afe6ac5d93c79751660f6047');
INSERT INTO `sys_role_frontend_menu_table` VALUES (200, '1', 'be73e07969d1a6d3c0891f96c43541d3');
INSERT INTO `sys_role_frontend_menu_table` VALUES (201, '1', '3bbde7e74e4d5db63e17cd6a84b4bd07');
INSERT INTO `sys_role_frontend_menu_table` VALUES (202, '1', 'ff3a02a29bf129fece80501c1cb4c999');
INSERT INTO `sys_role_frontend_menu_table` VALUES (203, '1', '8b3d9cff2d59532f8034802c594113f8');
INSERT INTO `sys_role_frontend_menu_table` VALUES (204, '1', '60ecb6f36d0c564cf529bd3ab690868a');
INSERT INTO `sys_role_frontend_menu_table` VALUES (205, '1', '082a4ae846138f375f7ac0d4368f5f6a');
INSERT INTO `sys_role_frontend_menu_table` VALUES (206, '1', '797b2e7d4f27ac9b20ecfd1becd41443');
INSERT INTO `sys_role_frontend_menu_table` VALUES (207, '1', 'facf38577c03dca949bc7979106976f7');
INSERT INTO `sys_role_frontend_menu_table` VALUES (208, '1', '32870cd6495e266bf5dae9b667f92868');
INSERT INTO `sys_role_frontend_menu_table` VALUES (209, '1', 'bd623651825b5111be1b15105618dfb4');
INSERT INTO `sys_role_frontend_menu_table` VALUES (210, '1', '62d4bf7a955f02b18cd6c40a89f980fb');
INSERT INTO `sys_role_frontend_menu_table` VALUES (211, '1', '3ba716cafd033b6455b50db61515cf01');
INSERT INTO `sys_role_frontend_menu_table` VALUES (212, '1', '13f84b1266f9a5ba00c751b2278a9d87');
INSERT INTO `sys_role_frontend_menu_table` VALUES (213, '1', 'd0921711f854419f3835994eb706ac96');
INSERT INTO `sys_role_frontend_menu_table` VALUES (214, '1', 'fb43f13e9bc0b8f39e1a49b6d532ecb5');
INSERT INTO `sys_role_frontend_menu_table` VALUES (215, '1', 'a153ff5458f214762c758e28277527c8');
INSERT INTO `sys_role_frontend_menu_table` VALUES (216, '1', '316a871b74401eef579073f559cd2add');
INSERT INTO `sys_role_frontend_menu_table` VALUES (217, '1', '775571c7a7312267c2f8c5e90106710e');
INSERT INTO `sys_role_frontend_menu_table` VALUES (218, '1', 'b3dfd292f4a398c7a502ccba02aa1abe');
INSERT INTO `sys_role_frontend_menu_table` VALUES (222, '1561a0e487e753eaf59ea903304b0147', 'ff3a02a29bf129fece80501c1cb4c999');
INSERT INTO `sys_role_frontend_menu_table` VALUES (223, '1561a0e487e753eaf59ea903304b0147', '3bbde7e74e4d5db63e17cd6a84b4bd07');
INSERT INTO `sys_role_frontend_menu_table` VALUES (224, '1561a0e487e753eaf59ea903304b0147', 'bd623651825b5111be1b15105618dfb4');
INSERT INTO `sys_role_frontend_menu_table` VALUES (225, '1561a0e487e753eaf59ea903304b0147', 'a153ff5458f214762c758e28277527c8');

-- ----------------------------
-- Table structure for sys_role_user_table
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user_table`;
CREATE TABLE `sys_role_user_table`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色ID',
  `user_id` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_user_table
-- ----------------------------
INSERT INTO `sys_role_user_table` VALUES (68, '1', '6');
INSERT INTO `sys_role_user_table` VALUES (69, '1', '626af5a6d91090ce00ab9eb247946913');
INSERT INTO `sys_role_user_table` VALUES (70, '1', '6b0e11af2dd82e37ae3001da192c4332');
INSERT INTO `sys_role_user_table` VALUES (73, '1561a0e487e753eaf59ea903304b0147', '15f508ccee719c57545c2b5093555a02');
INSERT INTO `sys_role_user_table` VALUES (74, '1561a0e487e753eaf59ea903304b0147', '6');
INSERT INTO `sys_role_user_table` VALUES (75, 'a1abb41df89147cb72ef19fc977ce261', '6');
INSERT INTO `sys_role_user_table` VALUES (76, 'a1abb41df89147cb72ef19fc977ce261', '6b0e11af2dd82e37ae3001da192c4332');

-- ----------------------------
-- Table structure for t_department
-- ----------------------------
DROP TABLE IF EXISTS `t_department`;
CREATE TABLE `t_department`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `address` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_department
-- ----------------------------
INSERT INTO `t_department` VALUES (1, 'A栋303', '1001', '呼吸内科', '2021-07-21 19:14:53', '内科');
INSERT INTO `t_department` VALUES (2, 'A栋404', '1002', '消化内科', '2021-07-20 19:15:33', '内科');
INSERT INTO `t_department` VALUES (3, 'B栋101', '1003', '泌尿内科', '2021-07-22 17:24:49', '内科');
INSERT INTO `t_department` VALUES (4, 'C栋102', '1004', '神经外科', '2021-07-22 17:28:08', '外科');
INSERT INTO `t_department` VALUES (5, 'C栋208', '1005', '整形外科', '2021-07-22 17:28:38', '外科');
INSERT INTO `t_department` VALUES (6, 'B栋104', '1008', '体检', '2021-07-22 17:29:03', '门诊');

-- ----------------------------
-- Table structure for t_doctor
-- ----------------------------
DROP TABLE IF EXISTS `t_doctor`;
CREATE TABLE `t_doctor`  (
  `id` int(0) NOT NULL,
  `doctor_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `department_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `consultation_fee` int(0) NULL DEFAULT NULL,
  `quota` int(0) NULL DEFAULT NULL,
  `registered` int(0) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_doctor
-- ----------------------------
INSERT INTO `t_doctor` VALUES (1, '210721', '1001', '上午九点到下午四点', 10, 30, 10, '李医生');
INSERT INTO `t_doctor` VALUES (2, '210719', '1002', '上午八点半到下午五点', 20, 40, 20, '王医生');
INSERT INTO `t_doctor` VALUES (3, '210818', '1003', '上午八点到下午六点', 20, 50, 40, '赵医生');
INSERT INTO `t_doctor` VALUES (4, '230987', '1003', '上午八点到下午三点', 20, 30, 30, '张医生');
INSERT INTO `t_doctor` VALUES (5, '325689', '1005', '上午九点到下午四点', 20, 30, 30, ' 黄医生');
INSERT INTO `t_doctor` VALUES (6, '209789', '1008', '上午八点到下午六点', 10, 50, 50, '陈医生');
INSERT INTO `t_doctor` VALUES (7, '352678', '1004', '上午九点到下午五点', 20, 30, 20, '于医生');

-- ----------------------------
-- Table structure for t_drug
-- ----------------------------
DROP TABLE IF EXISTS `t_drug`;
CREATE TABLE `t_drug`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `drug_type_code` int(0) NULL DEFAULT NULL,
  `efficacy_classification_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `manufacturer` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL,
  `specification` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `unit` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `product_time` date NULL DEFAULT NULL,
  `quality_date` date NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_drug
-- ----------------------------
INSERT INTO `t_drug` VALUES (1, 1, '2', '西南药业股份有限公司', '布洛芬缓释片', 120, '0.3g*20', '盒', 32.50, '2020-01-01', '2021-06-12', '2021-08-02 17:53:50');
INSERT INTO `t_drug` VALUES (13, 3, '1', '葛兰素史克有限公司', '拉米夫定', 220, '100mg*14', '盒', 98.00, '2000-02-29', '2021-06-30', '2021-07-22 18:52:02');
INSERT INTO `t_drug` VALUES (14, 8, '5', '北京暖胃宝有限公司', '暖胃宝', 280, '100mg每包*20', '盒', 22.50, '2020-02-02', '2021-04-02', '2021-07-23 10:04:21');
INSERT INTO `t_drug` VALUES (15, 2, '6', '华东药品生产有限公司', '止疼片', 89, '20mg每片*30', '瓶', 99.50, '2019-12-31', '2021-12-30', '2021-07-23 10:05:30');
INSERT INTO `t_drug` VALUES (18, 1, '2', '四川蜀中制药有限公司', '复方氨酚烷胺', 110, '0.3g*10', '板', 34.50, '2020-02-01', '2021-07-12', '2021-07-21 17:28:03');
INSERT INTO `t_drug` VALUES (19, 2, '1', '成都市潜江制药有限公司', '罗痛定片', 240, '0.3g*20', '板', 35.60, '2020-02-01', '2021-08-01', '2021-08-05 20:46:09');
INSERT INTO `t_drug` VALUES (20, 2, '6', '北京制药公司', '胃康宁', 46, '30mg*15', '板', 102.00, '2021-07-20', '2022-07-22', '2021-07-23 16:26:29');
INSERT INTO `t_drug` VALUES (21, 2, '7', '阿莫西林生产制作商', '阿莫西林', 170, '25mg*12片一板，一盒两板', '盒', 110.00, '2020-05-07', '2021-07-30', '2021-08-09 19:00:34');

-- ----------------------------
-- Table structure for t_drug_type
-- ----------------------------
DROP TABLE IF EXISTS `t_drug_type`;
CREATE TABLE `t_drug_type`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `drug_type_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `drug_type_code` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_drug_type
-- ----------------------------
INSERT INTO `t_drug_type` VALUES (1, '胶囊', 1, '2021-08-02 17:55:22');
INSERT INTO `t_drug_type` VALUES (2, '药片', 2, '2021-07-22 11:07:49');
INSERT INTO `t_drug_type` VALUES (3, '试剂', 3, '2021-07-22 11:08:08');
INSERT INTO `t_drug_type` VALUES (4, '药水', 4, '2021-07-22 11:10:18');
INSERT INTO `t_drug_type` VALUES (5, '冲剂', 8, '2021-07-22 18:56:24');
INSERT INTO `t_drug_type` VALUES (6, '药丸', 9, '2021-07-23 09:55:38');

-- ----------------------------
-- Table structure for t_efficacy_classification
-- ----------------------------
DROP TABLE IF EXISTS `t_efficacy_classification`;
CREATE TABLE `t_efficacy_classification`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `efficacy_classification_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `efficacy_classification_code` int(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_efficacy_classification
-- ----------------------------
INSERT INTO `t_efficacy_classification` VALUES (1, '退热', 1, '2021-07-21 14:33:58');
INSERT INTO `t_efficacy_classification` VALUES (2, '解热镇痛', 2, '2021-07-21 16:29:41');
INSERT INTO `t_efficacy_classification` VALUES (3, '防流感', 3, '2021-07-21 16:15:40');
INSERT INTO `t_efficacy_classification` VALUES (5, '暖胃', 5, '2021-07-22 18:49:01');
INSERT INTO `t_efficacy_classification` VALUES (6, '止痛', 6, '2021-07-22 18:59:39');
INSERT INTO `t_efficacy_classification` VALUES (7, '消炎', 7, '2021-07-23 09:55:50');

-- ----------------------------
-- Table structure for t_examination
-- ----------------------------
DROP TABLE IF EXISTS `t_examination`;
CREATE TABLE `t_examination`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `blood_pressure` int(0) NULL DEFAULT NULL,
  `body_temperature` double NULL DEFAULT NULL,
  `heart_rate` int(0) NULL DEFAULT NULL,
  `cost` decimal(10, 2) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `card_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `pay_status` int(1) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `pulse` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_examination
-- ----------------------------
INSERT INTO `t_examination` VALUES (1, 40, 36, 40, 20.00, '2021-08-05 15:03:35', '250753', 1, 40);
INSERT INTO `t_examination` VALUES (3, 32, 37, 50, 25.00, '2021-08-05 16:14:28', '261992', 1, 48);
INSERT INTO `t_examination` VALUES (5, 38, 36, 56, 25.00, '2021-08-05 17:04:02', '453900', 1, 42);
INSERT INTO `t_examination` VALUES (6, 45, 36.7, 48, 20.00, '2021-08-06 10:19:57', '843980', 1, 45);

-- ----------------------------
-- Table structure for t_notice
-- ----------------------------
DROP TABLE IF EXISTS `t_notice`;
CREATE TABLE `t_notice`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `contents` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_notice
-- ----------------------------

-- ----------------------------
-- Table structure for t_outpatient
-- ----------------------------
DROP TABLE IF EXISTS `t_outpatient`;
CREATE TABLE `t_outpatient`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `condition_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `diagnosis_result` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `drug_cost` decimal(10, 2) NOT NULL,
  `medical_order` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `prescription_num` int(0) NULL DEFAULT NULL,
  `card_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `pay_status` int(1) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `doctor_order` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_outpatient
-- ----------------------------
INSERT INTO `t_outpatient` VALUES (1, '轻度感冒', '轻度感冒', 97.50, '每天三次', 11471883, '250753', '2021-08-05 10:42:33', 1, '多喝热水');
INSERT INTO `t_outpatient` VALUES (2, '111', '222', 398.00, '无', 13503933, '453900', '2021-08-05 17:06:19', 1, '333');
INSERT INTO `t_outpatient` VALUES (5, '有病', '有病', 673.50, '每日三次', 52386771, '843980', '2021-08-06 12:19:06', 1, '有病');

-- ----------------------------
-- Table structure for t_patient
-- ----------------------------
DROP TABLE IF EXISTS `t_patient`;
CREATE TABLE `t_patient`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `address` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `card_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `identity_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `nationality` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `telephone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `personal_history` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `family_history` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_patient
-- ----------------------------
INSERT INTO `t_patient` VALUES (1, '中央大街一号', '1999-09-09', '250753', '331002199909090987', '张三', '汉族', '男', '15678889087', '2021-08-05 10:00:44', '无个人病史', '无家族病史');
INSERT INTO `t_patient` VALUES (2, '中央大街2号', '1999-07-07', '261992', '33176219990707781X', '李四', '汉族', '男', '18768987954', '2021-08-05 15:54:31', NULL, NULL);
INSERT INTO `t_patient` VALUES (3, '中央大街3号', '1998-09-13', '453900', '331007199809138976', '王五', '汉族', '男', '13657890120', '2021-08-05 16:50:06', NULL, NULL);
INSERT INTO `t_patient` VALUES (4, 'xxx街道', '2000-10-10', '843980', '33100120001010091X', '阿猫', '汉族', '男', '13678861222', '2021-08-06 10:16:01', NULL, NULL);

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` int(10) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_permission
-- ----------------------------

-- ----------------------------
-- Table structure for t_register
-- ----------------------------
DROP TABLE IF EXISTS `t_register`;
CREATE TABLE `t_register`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `card_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `operator_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `payment` int(0) NULL DEFAULT NULL,
  `doctor_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_register
-- ----------------------------
INSERT INTO `t_register` VALUES (1, '1002', '250753', 'admin', '2021-08-09 11:27:30', 220, '210719');
INSERT INTO `t_register` VALUES (3, '1008', '261992', 'admin', '2021-08-09 11:27:30', 210, '209789');
INSERT INTO `t_register` VALUES (4, '1008', '453900', 'admin', '2021-08-09 11:27:30', 210, '209789');
INSERT INTO `t_register` VALUES (5, '1008', '843980', 'admin', '2021-08-09 11:27:30', 210, '209789');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', 'ROLE_ADMIN', '管理员', '2021-07-31 15:56:27');
INSERT INTO `t_role` VALUES ('1561a0e487e753eaf59ea903304b0147', 'ROLE_DOCTOR', '医生', '2021-08-09 17:14:04');
INSERT INTO `t_role` VALUES ('a1abb41df89147cb72ef19fc977ce261', 'ROLE_USER', '用户', '2021-08-08 15:27:28');
INSERT INTO `t_role` VALUES ('d304a2c2d498d66f785fc9ce90f1d16d', 'ROLE_Register', '挂号员', '2021-08-09 20:58:11');
INSERT INTO `t_role` VALUES ('d8ef14cde309d4708a8714ba96f1ecff', 'ROLE_Examer', '体检员', '2021-08-09 20:57:48');

-- ----------------------------
-- Table structure for t_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_role_permission`;
CREATE TABLE `t_role_permission`  (
  `rid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `pid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`rid`, `pid`) USING BTREE,
  INDEX `fk_pid`(`pid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_permission
-- ----------------------------

-- ----------------------------
-- Table structure for t_take_drug
-- ----------------------------
DROP TABLE IF EXISTS `t_take_drug`;
CREATE TABLE `t_take_drug`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `prescription_num` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `take_time` datetime(0) NULL DEFAULT NULL,
  `take_status` int(1) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `card_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `drug_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `operator_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `drug_num` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_take_drug
-- ----------------------------
INSERT INTO `t_take_drug` VALUES (4, '11471883', '2021-08-05 19:33:27', 1, '250753', '2021-08-05 19:33:27', '1', 'yxh', 3);
INSERT INTO `t_take_drug` VALUES (5, '13503933', '2021-08-05 19:37:07', 1, '453900', '2021-08-05 19:37:06', '15', 'yxh', 4);
INSERT INTO `t_take_drug` VALUES (9, '52386771', '2021-08-06 12:53:41', 1, '843980', '2021-08-06 12:53:41', '1', 'admin', 3);
INSERT INTO `t_take_drug` VALUES (10, '52386771', '2021-08-06 12:53:41', 1, '843980', '2021-08-06 12:53:41', '15', 'admin', 4);
INSERT INTO `t_take_drug` VALUES (11, '52386771', '2021-08-06 12:53:41', 1, '843980', '2021-08-06 12:53:41', '19', 'admin', 5);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `pass_word` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `role_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `work_address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `plain_password` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `state` int(1) UNSIGNED ZEROFILL NULL DEFAULT 1,
  `department_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('15f508ccee719c57545c2b5093555a02', 'yxh', '$2a$10$rRcwWgIVgYso9Be0B8Uu2uG/GGBClNSMt6ZdhoVHZezyTUcExlwci', '2021-08-09 01:15:25', '1', '12464204@qq.com', '17664153931', '北京', '男', '123456', '2021-08-08', 1, '2');
INSERT INTO `t_user` VALUES ('1e436eb5d29d11dfa3770937e5831b5c', '12wa1111', '$2a$10$TOrOj/rfBruqNbPU.Ya2Ne3kOA9b0aKT.lAnj4mMO1N38aEoImXvC', '2021-08-10 10:27:16', NULL, '112312@qq.com', NULL, NULL, NULL, '123456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES ('332d0c2f86abd2902ce044653d463312', 'sd', '$2a$10$ea0hz3rGNDANR7QsMk1JP.RdmA4Bdt.FfyZDd.dpBEOcns3s6tl0O', '2021-08-09 01:12:18', NULL, '12345@qq.com', '17664153931', '上海', '男', '123', '2021-08-08', 1, '2');
INSERT INTO `t_user` VALUES ('6', 'yuxiaoqiang', '$2a$10$k/uIWfm3L3iEXJjPOqZxV.OjrWCdzyCgjXHzvwrQGX0/xpzLgdz4S', '2021-08-02 18:58:49', '1', NULL, NULL, NULL, NULL, '123456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES ('626af5a6d91090ce00ab9eb247946913', 'yuxiao', '$2a$10$7jJBCfaIvgRGTTi.ByJRM.wByrbbL7T3dCjM5vL7oCelbmIa/QwDO', '2021-08-04 17:17:23', NULL, NULL, NULL, NULL, NULL, '123456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES ('6b0e11af2dd82e37ae3001da192c4332', 'ada', '$2a$10$zMr5UtJlR4.EyPPOkbudaON4V6efolWTlEgezfhuuejmT0B/t6MsW', '2021-08-09 01:09:42', NULL, '123@qq.com', '17664153931', '上海', '男', '123456', '2021-08-02', 1, '2');
INSERT INTO `t_user` VALUES ('8', 'admin', '$2a$10$FJq8CP6iGzYaKsebhNdbfute7kR2cyAAvBIst4tehVHjYrQBp5Joy', '2021-08-09 21:21:23', '1', '1321313132@qq.com', '13876543211', 'sfafa', '男', '123456', '2000-02-01', 1, NULL);
INSERT INTO `t_user` VALUES ('9273665d9cf54bed64d85c7f9cd81eb2', 'yuxiaoqiang2', '$2a$10$IhFf4T3mLMSkkt9CmPm89ehmnETg7xymV26cgk76qTpsXtBv24T/e', '2021-07-30 14:20:37', NULL, NULL, NULL, NULL, NULL, '123456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES ('a802c5176b0c431708437726b609e87a', 'yuxiaoqiang1', '$2a$10$tD9M6yesLSsxcHGjDdPKnegdDpAAxqQHQFgAgnQuo1GaBKGzyRgU.', NULL, NULL, NULL, NULL, NULL, NULL, '123456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES ('c8a3266f7ad073e60237462b7f5cae5a', 'saww1111', '$2a$10$ARgai/ilP3R/c4Xiu1Xpe.tXO9Yu0gcaSJVHkcnhbNWIL/6LxGoJ6', '2021-08-10 10:23:53', NULL, '112312@qq.com', NULL, NULL, NULL, '123456', NULL, 1, NULL);
INSERT INTO `t_user` VALUES ('f15c4ee6eaaa152bac045ea76ff758fa', 'test1234', '$2a$10$7G2Jj2kGqdfmwvvSVpEe3e7NWdmTNmF.f9XirepdBJ3uY5ZTwJToK', '2021-08-10 10:33:41', NULL, '', NULL, NULL, NULL, '123456', NULL, 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
