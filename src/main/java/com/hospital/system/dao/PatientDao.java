package com.hospital.system.dao;

import com.hospital.system.model.Patient;
import org.apache.ibatis.annotations.Param;


public interface PatientDao {

    int insertPatient(Patient patient);

    Patient selectPatient(String cardId);

    int updatePatient(@Param("cardId") String cardId,@Param("personalHistory") String personalHistory, @Param("familyHistory") String familyHistory);
}
