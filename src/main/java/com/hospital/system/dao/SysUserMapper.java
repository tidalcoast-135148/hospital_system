package com.hospital.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.system.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import javax.management.relation.Role;
import java.util.List;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    @Select("SELECT role_name from t_role where role_id in (select role_id from sys_role_user_table where user_id=#{user_id})")
    public List<String> getRoleNameByUserId(@Param("user_id")String userId);
}
