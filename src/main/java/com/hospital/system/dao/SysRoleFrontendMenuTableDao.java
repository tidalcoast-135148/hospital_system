package com.hospital.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.system.entity.SysRoleFrontendMenuTable;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleFrontendMenuTableDao extends BaseMapper<SysRoleFrontendMenuTable> {

}