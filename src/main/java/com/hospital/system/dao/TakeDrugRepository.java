package com.hospital.system.dao;

import com.hospital.system.model.Drug;
import com.hospital.system.model.TakeDrug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Transactional
@Repository
public interface  TakeDrugRepository extends JpaRepository<TakeDrug, Integer>, JpaSpecificationExecutor<TakeDrug> {


    @Modifying
    @Query(value = "update t_take_drug  set take_status= 1, take_time = :takeTime  where prescription_num = :pNum", nativeQuery = true)
    int updateBypNum(@Param("pNum") String pNum, @Param("takeTime") Date takeTime);


    @Query(value = "select * from t_take_drug where prescription_num = :pNum", nativeQuery = true)
    List<TakeDrug> selectBypNum(@Param("pNum") String pNum);

    @Query(value = "select * from t_take_drug where card_id = :cardId", nativeQuery = true)
    List<TakeDrug> selectByCardId(@Param("cardId") String cardId);

}
