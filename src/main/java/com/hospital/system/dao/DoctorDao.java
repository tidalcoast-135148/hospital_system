package com.hospital.system.dao;

import com.hospital.system.model.Doctor;

import java.util.List;

public interface DoctorDao {
    List<Doctor> selectDoctorsByDepartmentCode(String code);
    Doctor selectDoctorById(String doctorCode);
}
