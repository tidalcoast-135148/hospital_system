package com.hospital.system.dao;

import com.hospital.system.model.Register;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegisterDao {
    int insertRegister(Register register);
    List<Register> selectRegister();
    Register selectOneRegister(String cardId);
    List<Register> selectRegisterForPage(int page);
    List<Register> selectRegisterByDepartmentCode(String department);
}
