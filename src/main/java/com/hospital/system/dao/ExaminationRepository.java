package com.hospital.system.dao;

import com.hospital.system.model.Examination;
import org.hibernate.sql.Update;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ExaminationRepository extends JpaSpecificationExecutor<Examination>, JpaRepository<Examination, Integer> {

    @Modifying
    @Query("update Examination e set e.payStatus = 1 where e.cardId = :cardId")
    int updatePayStatus(@Param("cardId") String cardId);

    @Query(value = "select * from t_examination where card_id = :cardId", nativeQuery = true)
    Examination selectByCardId(@Param("cardId") String cardId);

    @Modifying
    @Query("update Examination e set e.bloodPressure = :bloodPressure, e.bodyTemperature = :bodyTemperature," +
            "e.cost = :cost, e.heartRate = :heartRate, e.pulse = :pulse where e.cardId = :cardId")
    int update(@Param("cardId") String cardId, @Param("bloodPressure") Integer bloodPressure, @Param("bodyTemperature") Double bodyTemperature,
               @Param("cost") Double cost, @Param("heartRate") Integer heartRate, @Param("pulse") Integer pulse);
}
