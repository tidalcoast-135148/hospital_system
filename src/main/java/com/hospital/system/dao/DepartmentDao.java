package com.hospital.system.dao;

import com.hospital.system.model.Department;

import java.util.List;

public interface DepartmentDao {

    Department selectDepartmentByName( String name);

    Department selectDepartmentByCode(String departmentCode);

    List<Department> selectAll();
}
