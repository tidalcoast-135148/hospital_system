package com.hospital.system.dao;

import com.hospital.system.model.Drug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

@Transactional
@Repository
public interface DrugRepository extends JpaRepository<Drug, Integer>, JpaSpecificationExecutor<Drug> {

    /**
     根据id更新
     */
    @Modifying
    @Query("update Drug d set d.typeCode = :typeCode, d.efficacyCode = :efficacyCode, d.manufacturer = :manufacturer, d.name = :name, " +
            "d.num = :num, d.specification = :specification, d.unit = :unit, d.price = :price where d.id=:id")
    int update(@Param("typeCode") Integer typeCode, @Param("efficacyCode") Integer efficacyCode, @Param("manufacturer") String manufacturer, @Param("name") String name,
               @Param("num") Integer num, @Param("specification") String specification, @Param("unit") String unit, @Param("price") Double price, @Param("id") Integer id);



    @Modifying
    @Query("delete from Drug d where d.typeCode = ?1")
    int deleteByTypeCode(@Param("typeCode") Integer code);

    @Modifying
    @Query("delete from Drug d where d.efficacyCode = ?1")
    int deleteByEfficacyCode(@Param("efficacyCode") Integer code);

    @Modifying
    @Query("update  Drug d set d.num = :num where d.id = :id")
    int updateNum(@Param("id") Integer id, @Param("num") Integer num);


}
