package com.hospital.system.dao;

import com.hospital.system.model.DrugType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface DrugTypeRepository extends JpaRepository<DrugType,Integer>, JpaSpecificationExecutor<DrugType> {

    @Modifying
    @Query("update DrugType dt set dt.name = :name where dt.id=:id")
    int updateById(@Param("id") Integer id, @Param("name") String name);

    @Modifying
    @Query("delete from DrugType dt where dt.id=:id")
    void deleteById(@Param("id") Integer id);

    @Modifying
    @Query("delete from DrugType dt where dt.code=:code")
    int deleteByCode(@Param("code") Integer code);


}
