package com.hospital.system.dao;

import com.hospital.system.model.OutPatient;

public interface OutPatientDao {
    int insertOutPatient(OutPatient outPatient);
    OutPatient selectOutPatient(String cardId);
    int updateOutPatient(String cardId);
}
