package com.hospital.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.system.entity.SysRoleUserTable;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleUserTableDao extends BaseMapper<SysRoleUserTable> {

}