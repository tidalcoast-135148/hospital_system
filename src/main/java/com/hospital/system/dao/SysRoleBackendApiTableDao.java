package com.hospital.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.system.entity.SysRoleBackendApiTable;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleBackendApiTableDao extends BaseMapper<SysRoleBackendApiTable> {

}