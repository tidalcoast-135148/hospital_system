package com.hospital.system.dao;

import com.hospital.system.model.Efficacy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Transactional
@Repository
public interface EfficacyRepository extends JpaSpecificationExecutor<Efficacy>, JpaRepository<Efficacy, Integer> {

    Efficacy findByName(String name);

    /**
     * 更新和删除操作需要加@Modifying和@Transactional
     * @param id
     * @param name
     * @return
     */
    @Modifying
    @Query("update Efficacy e set e.name = :name where e.id=:id")
    int updateById(@Param("id") Integer id, @Param("name") String name);

    @Modifying
    @Query("delete from Efficacy e where e.id=:id")
    void deleteById(@Param("id") Integer id);

    @Modifying
    @Query("delete from Efficacy e where e.code=:code")
    int deleteByCode(@Param("code") Integer code);
}
