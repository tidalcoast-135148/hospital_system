package com.hospital.system.model;

import lombok.Data;

import java.util.Date;

@Data
public class Patient {

    private int id;
    private String name;
    private String sex;
    private String identityCard;
    private String address;
    private Date birthday;
    private String cardId;
    private String nationality;
    private String telephone;
    private Date createTime;
    private String personalHistory;
    private String familyHistory;

}
