package com.hospital.system.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;


import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "t_examination")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer" , "handler"})
public class  Examination {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "blood_pressure")
    private Integer bloodPressure;

    @Column(name = "heart_rate")
    private Integer heartRate;

    @Column(name = "body_temperature")
    private Double bodyTemperature;

    @Column(name = "pulse")
    private Integer pulse;

    @Column(name = "cost")
    private Double cost;

    @Column(name = "card_id")
    private String cardId;

    @Column(name = "pay_status")
    private int payStatus;

    @CreatedDate
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date createTime;

}
