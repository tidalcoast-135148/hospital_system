package com.hospital.system.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "t_take_drug")
@EntityListeners(AuditingEntityListener.class)
public class TakeDrug {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "prescription_num")
    private String prescriptionNum;

    @Column(name = "operator_name")
    private String operatorName;

    @Column(name = "take_status")
    private int takeStatus;

    @Column(name = "card_id")
    private String cardId;

    @Column(name = "drug_id")
    private Integer drugId;

    @Column(name = "take_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date takeTime;

    @Column(name = "create_time")
    @CreatedDate
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Column(name = "drug_num")
    private Integer drugNum;

    //忽略字段，用于多表联合查询
    @Transient
    private int payStatus;

    @Transient
    private String drugName;

    @Transient
    private String username;

    @Transient
    private Double price;

    @Transient
    private String specification;

    @Transient
    private String drugInfo;



}
