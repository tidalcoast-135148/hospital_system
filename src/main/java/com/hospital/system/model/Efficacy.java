package com.hospital.system.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "t_efficacy_classification")
@EntityListeners(AuditingEntityListener.class)
public class Efficacy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "efficacy_classification_name", length = 20)
    private String name;

    @Column(name = "efficacy_classification_code")
    private Integer code;

    @Column(name = "create_time")
    @CreatedDate
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date createTime;


}
