package com.hospital.system.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "t_drug")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer" , "handler"})
@EntityListeners(AuditingEntityListener.class)
public class Drug {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "drug_type_code")
    private Integer typeCode;//药品的类型id

    @Column(name = "efficacy_classification_code")
    private Integer efficacyCode;//药品功效类型id

    @Column(name = "manufacturer")
    private String manufacturer;//制作商

    @Column(name = "name")
    private String name;

    @Column(name = "num")
    private Integer num;//库存数量

    @Column(name = "specification")
    private String specification;//药品规格

    @Column(name = "unit")
    private String unit;//药品包装单位

    @Column(name = "price")
    private double price;//药品价格

    @Column(name = "product_time")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date productTime;//生产时间

    @Column(name = "quality_date")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date qualityDate;//药品保质期日期

    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date createTime;

    @Transient
    private String typeName;

    @Transient
    private String efficacyName;
}
