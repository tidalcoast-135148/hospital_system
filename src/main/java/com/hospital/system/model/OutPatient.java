package com.hospital.system.model;

import lombok.Data;

import java.util.Date;

@Data
public class OutPatient {
    private int id;
    private String conditionDescription;
    private String diagnosisResult;
    private String drugCost;
    private String medicalOrder;
    private String prescriptionNum;
    private String cardId;
    private Date createTime;
    private int payStatus;
    private String doctorOrder;
}
