package com.hospital.system.controller;

import com.hospital.system.model.DrugType;
import com.hospital.system.service.impl.DrugTypeServiceImpl;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("drugType")
public class DrugTypeController {

    @Resource
    private DrugTypeServiceImpl drugTypeServiceImpl;

    @GetMapping("selectAll")
    public List<Map<String, Object>> findAll(){
        List<DrugType> drugTypeList =  drugTypeServiceImpl.findAll();
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (DrugType dt : drugTypeList) {
            Map<String, Object> typeMap = new HashMap<>();
            typeMap.put("value", dt.getCode());
            typeMap.put("label", dt.getName());
            resultList.add(typeMap);
        }
        return resultList;
    }


    @PostMapping("insert")
    public String insert(@RequestBody DrugType drugType){
        if(drugTypeServiceImpl.insert(drugType.getName()))
            return "success";
        return "fail";
    }

    @PostMapping("update")
    public String update(Integer id, String name){
        if(drugTypeServiceImpl.updateById(id, name) > 0)
            return "success";
        return "fail";
    }

    @PostMapping("delete")
    public String delete(@RequestBody DrugType drugType){
        if(drugTypeServiceImpl.deleteByCode(drugType.getCode()) > 0)
            return "success";
        return "fail";
    }


}