package com.hospital.system.controller;

import com.hospital.system.model.OutPatient;
import com.hospital.system.model.Patient;
import com.hospital.system.result.Result;
import com.hospital.system.service.OutPatientService;
import com.hospital.system.service.PatientService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("clinic")
public class ClinicController {

    @Resource
    private PatientService patientService;

    @Resource
    private OutPatientService outPatientService;

    //    给病人添加个人史，家族史
    @RequestMapping("updatePatient")
    public Result updatePatient(String cardId, String personalHistory, String familyHistory) {
        System.out.println(cardId);
        System.out.println(personalHistory);
        System.out.println(familyHistory);
        boolean b = patientService.updatePatient(cardId, personalHistory, familyHistory);
        return b ? Result.success("success") : Result.fail(1, "更新失败！");
    }
    //    生成处方号
    @RequestMapping("generatePrescriptionNum")
    public Result generatePrescriptionNum(){
        int pN = (int)((Math.random()*9+1)*10000000);
        Patient p = patientService.selectPatient(Integer.toString(pN));
        if(p!=null){
           generatePrescriptionNum();
        }
        return new Integer(pN)!=null?Result.success(pN):Result.fail(1,"读取失败！");
    }
    //插入门诊信息
    @RequestMapping("insertOutPatient")
    public Result insertOutPatient(@RequestBody OutPatient outPatient){
        return outPatientService.insertOutPatient(outPatient)?Result.success("success"):Result.fail(1,"插入门诊信息失败！");
    }
//    根据卡号查询门诊表
    @RequestMapping("selectOutPatient")
    public Result selectOutPatient(String cardId){
        OutPatient op = outPatientService.selectOutPatient(cardId);
        return op!=null?Result.success(op):Result.fail(1,"查询失败!");
    }
}
