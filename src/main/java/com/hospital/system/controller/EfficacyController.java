package com.hospital.system.controller;

import com.hospital.system.model.Efficacy;
import com.hospital.system.service.impl.EfficacyServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("drugEfficacy")
public class EfficacyController {

    @Resource
    private EfficacyServiceImpl efficacyServiceImpl;

    @GetMapping("selectAll")
    public List<Map<String, Object>> selectAll() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<Efficacy> efficacyList = efficacyServiceImpl.findAll();
        for (Efficacy e : efficacyList) {
            Map<String, Object> efficacyMap = new HashMap<>();
            efficacyMap.put("value", e.getCode());
            efficacyMap.put("label", e.getName());
            resultList.add(efficacyMap);
        }
        return resultList;
    }

    @GetMapping("selectByName")
    public Efficacy selectByName(@RequestParam("name") String name) {
        return efficacyServiceImpl.findByName(name);
    }

    @PostMapping("insert")
    public String inset(@RequestBody Efficacy efficacy) {
        if (efficacyServiceImpl.insert(efficacy.getName())) {
            return "success";
        }
        return "fail";
    }

    @PostMapping("update")
    public String update(@RequestParam("id") Integer id, @RequestParam("name") String name) {
        if (efficacyServiceImpl.updateById(id, name) > 0)
            return "success";
        return "fail";
    }

    @PostMapping("delete")
    public String delete(@RequestBody Efficacy efficacy) {
        if (efficacyServiceImpl.deleteByCode(efficacy.getCode()) > 0)
            return "success";
        return "fail";
    }
}
