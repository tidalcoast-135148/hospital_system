package com.hospital.system.controller;


import com.hospital.system.model.Drug;
import com.hospital.system.service.impl.DrugServiceImpl;
import com.hospital.system.service.impl.DrugTypeServiceImpl;
import com.hospital.system.service.impl.EfficacyServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("drug")
public class DrugController {

    @Resource
    private DrugServiceImpl drugServiceImpl;

    @Resource
    private DrugTypeServiceImpl drugTypeServiceImpl;

    @Resource
    private EfficacyServiceImpl efficacyServiceImpl;

    //药品分页 参数分别为名称，类型id，药效id，第几页，每页的数据条数
    @GetMapping("selectPage")
    public Page<Drug> getPage(String drugName, Integer typeCode, Integer efficacyCode, Integer pageNum, Integer pageSize) {
        Drug drug = new Drug();
        drug.setName(drugName);
        drug.setTypeCode(typeCode);
        drug.setEfficacyCode(efficacyCode);
        Page<Drug> drugPage = drugServiceImpl.findPage(drug, pageNum, pageSize);
        List<Drug> drugList = drugPage.getContent();
        for (Drug d : drugList) {
            String typeName = drugTypeServiceImpl.gainByCode(d.getTypeCode()).get(0).getName();
            d.setTypeName(typeName);
            String efficacyName = efficacyServiceImpl.gainByCode(d.getEfficacyCode()).get(0).getName();
            d.setEfficacyName(efficacyName);
        }
        return new PageImpl<>(drugList, PageRequest.of(pageNum, pageSize), drugPage.getTotalElements());
    }

    //获取所有药品
    @GetMapping("selectDrug")
    public List<Map<String, Object>> selectDrug(){
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<Drug> drugList =  drugServiceImpl.findAll();
        for(Drug d : drugList){
            Map<String, Object> drugMap = new HashMap<>();
            drugMap.put("value", d.getId());
            drugMap.put("label", d.getName());
            resultList.add(drugMap);
        }
        return resultList;
    }

    //根据名称模糊搜索药品
    @GetMapping("searchDrug")
    public List<Drug> searchDrug(String name){
        return drugServiceImpl.searchDrug(name);
    }

    //根据id查询药品
    @GetMapping("selectById")
    public Drug selectById(@Param("id") Integer id){
        Drug drug = drugServiceImpl.getOne(id);
        String typeName = drugTypeServiceImpl.gainByCode(drug.getTypeCode()).get(0).getName();
        String efficacyName = efficacyServiceImpl.gainByCode(drug.getEfficacyCode()).get(0).getName();
        drug.setTypeName(typeName);
        drug.setEfficacyName(efficacyName);
        return drug;
    }

    //添加药品
    @PostMapping("insert")
    public String insert(@RequestBody Drug drug) {
        if (drugServiceImpl.insert(drug)) {
            return "success";
        }
        return "fail";
    }

    //删除药品
    @PostMapping("delete")
    public String delete(@RequestBody Drug drug) {
        drugServiceImpl.deleteById(drug.getId());
        return "success";
    }

    //更新药品信息
    @PostMapping("update")
    public String update(@RequestBody Drug drug) {
        if (drugServiceImpl.update(drug.getTypeCode(), drug.getEfficacyCode(), drug.getManufacturer(),
                drug.getName(), drug.getNum(), drug.getSpecification(), drug.getUnit(), drug.getPrice(), drug.getId()) > 0) {
            return "success";
        }
        return "fail";
    }

    //药品出入库，修改库存数量
    @PostMapping("updateNum")
    public String updateNum(@RequestBody Drug drug) {
        if (drugServiceImpl.updateNum(drug.getId(), drug.getNum()) > 0) {
            return "success";
        }
        return "fail";
    }


}
