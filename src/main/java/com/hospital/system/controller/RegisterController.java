package com.hospital.system.controller;

import com.hospital.system.model.Department;
import com.hospital.system.model.Doctor;
import com.hospital.system.model.Patient;
import com.hospital.system.model.Register;
import com.hospital.system.model.combination.DoctorAndDepartment;
import com.hospital.system.model.combination.PatientAndDoctorAndDepartment;
import com.hospital.system.result.Result;
import com.hospital.system.service.DepartmentService;
import com.hospital.system.service.DoctorService;
import com.hospital.system.service.PatientService;
import com.hospital.system.service.RegisterService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("register")
public class RegisterController {


    @Resource
    private PatientService patientService;

    @Resource
    private DepartmentService departmentService;

    @Resource
    private DoctorService doctorService;

    @Resource
    private RegisterService registerService;

//    办理就诊卡
    @RequestMapping("register")
    public Result insertPatient(@RequestBody Patient patient){
        boolean result = patientService.insertPatient(patient);
        return result?Result.success(patient):Result.fail(1,"就诊卡办理失败！");
    }

//    生成就诊卡号
    @RequestMapping("generateCardId")
    public Result generateCardId(){
        int cardId = (int)((Math.random()*9+1)*100000);
        Patient p = patientService.selectPatient(Integer.toString(cardId));
        if(p!=null){
            generateCardId();
        }
        return new Integer(cardId)!=null?Result.success(cardId):Result.fail(1,"读取失败！");
    }

//    通过就诊卡号查询出病人的相关信息
    @RequestMapping("selectPatient")
    public Result selectPatient(String cardId){
        Patient patient = patientService.selectPatient(cardId);
        return patient!=null?Result.success(patient):Result.fail(1,"该卡号没有注册！");
    }

//    通过部门名称获得部门相关信息（部门类型，部门的编号）
    @RequestMapping("selectDepartmentByName")
    public Result selectDepartmentByName(String name){
        Department department = departmentService.selectDepartmentByName(name);
        return department!=null?Result.success(department):Result.fail(1,"查询部门失败！");
    }

//    通过部门的编号获得该部门所有医生
    @RequestMapping("selectDoctorsByDepartmentCode")
    public Result selectDoctorsByDepartmentCode(String code){
        List<Doctor> list = doctorService.selectDoctorsByDepartmentCode(code);
        return list!=null?Result.success(list):Result.fail(1,"该部门没有相关医生！");
    }

//    挂号
    @RequestMapping("insertRegister")
    public Result insertRegister(@RequestBody Register register){
        boolean result = registerService.insertRegister(register);
        return result?Result.success(register):Result.fail(1,"挂号失败！");
    }

//    查询个人挂号信息
    @RequestMapping("selectOneRegister")
    public Result selectOneRegister(String cardId){
        Register register = registerService.selectOneRegister(cardId);
        return register==null?Result.success(register):Result.fail(1,"该卡号不能重复挂号！");
    }

//    查询全部挂号信息或者分页
    @RequestMapping("selectRegister")
    public Result selectRegister(int page){
        List<PatientAndDoctorAndDepartment> l = new ArrayList();
        List<Register> list = new ArrayList<Register>();
        if(page==0){
             list = registerService.selectRegister();
        }else{
             list = registerService.selectRegisterForPage(page);
        }
        for(int i=0;i<list.size();i++){
            Patient p = patientService.selectPatient(list.get(i).getCardId());
            Doctor d = doctorService.selectDoctorById(list.get(i).getDoctorCode());
            Department de = departmentService.selectDepartmentByCode(list.get(i).getDepartmentCode());
            PatientAndDoctorAndDepartment pdd = new PatientAndDoctorAndDepartment();
            pdd.setCardId(list.get(i).getCardId());
            pdd.setName(p.getName());
            pdd.setDepartment(de.getName());
            pdd.setType(de.getType());
            pdd.setDoctor(d.getName());
            pdd.setCreateTime(list.get(i).getCreateTime());
            pdd.setOperator(list.get(i).getOperatorName());
            l.add(pdd);
        }
        return l!=null?Result.success(l):Result.fail(1,"查询挂号信息失败！");
    }

    @RequestMapping("selectRegisterByCardId")
    public Result selectRegisterByCardId(String cardId){
        Register register = registerService.selectOneRegister(cardId);
        Department department = departmentService.selectDepartmentByCode(register.getDepartmentCode());
        Doctor doctor = doctorService.selectDoctorById(register.getDoctorCode());
        DoctorAndDepartment dd = new DoctorAndDepartment();
        dd.setDepartment(department.getName());
        dd.setDoctor(doctor.getName());
        dd.setFee(doctor.getConsultationFee());
        return dd!=null?Result.success(dd):Result.fail(1,"查询失败！");
    }

//    根据department，cardId查询注册信息
    @RequestMapping("selectRegisterByDC")
    public Result selectRegisterByDC(String cardId,String department){
        List<PatientAndDoctorAndDepartment> l = new ArrayList();
        List<Register> list = new ArrayList<Register>();
        if(cardId!=null&&department==null){
            Register register = registerService.selectOneRegister(cardId);
            list.add(register);
        }
        if(department!=null&&cardId==null){
            String departmentCode = departmentService.selectDepartmentByName(department).getCode();
            list = registerService.selectRegisterByDepartmentCode(departmentCode);
        }
        for(int i=0;i<list.size();i++){
            Patient p = patientService.selectPatient(list.get(i).getCardId());
            Doctor d = doctorService.selectDoctorById(list.get(i).getDoctorCode());
            Department de = departmentService.selectDepartmentByCode(list.get(i).getDepartmentCode());
            PatientAndDoctorAndDepartment pdd = new PatientAndDoctorAndDepartment();
            pdd.setCardId(list.get(i).getCardId());
            pdd.setName(p.getName());
            pdd.setDepartment(de.getName());
            pdd.setType(de.getType());
            pdd.setDoctor(d.getName());
            pdd.setCreateTime(list.get(i).getCreateTime());
            pdd.setOperator(list.get(i).getOperatorName());
            l.add(pdd);
        }
        return l!=null?Result.success(l):Result.fail(1,"查询挂号信息失败！");
    }
}
