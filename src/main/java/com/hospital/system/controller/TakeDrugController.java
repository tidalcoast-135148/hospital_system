package com.hospital.system.controller;


import com.hospital.system.model.Drug;
import com.hospital.system.model.OutPatient;
import com.hospital.system.model.TakeDrug;
import com.hospital.system.service.OutPatientService;
import com.hospital.system.service.impl.DrugServiceImpl;
import com.hospital.system.service.impl.TakeDrugServiceImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("takeDrug")
public class TakeDrugController {

    @Resource
    private TakeDrugServiceImpl takeDrugServiceImpl;

    @Resource
    private DrugServiceImpl drugServiceImpl;

    @Resource
    private OutPatientService outPatientService;

    @PostMapping("insert")
    public String insert(@RequestBody TakeDrug takeDrug) {
        if (takeDrugServiceImpl.insert(takeDrug))
            return "success";
        return "fail";
    }

    @GetMapping("deleteAll")
    public String deleteAll() {
        takeDrugServiceImpl.deleteAll();
        return "success";
    }

    @PostMapping("updateStatus")
    public String updateStatus(@RequestBody TakeDrug takeDrug) {
        int result = takeDrugServiceImpl.updateBypNum(takeDrug.getPrescriptionNum(), new Date());
        if(result < 0){
            return "done";
        }
        if (result > 0) {
            return "success";
        }
        return "fail";
    }

    @GetMapping("selectTakeDrugPageByTimeDesc")//将查询结果按创建时间降序排序
    public Page<TakeDrug> selectTakeDrugPageByTimeDesc(String prescriptionNum, int pageNum, int pageSize) {
        TakeDrug takeDrug = new TakeDrug();
        takeDrug.setPrescriptionNum(prescriptionNum);
        Page<TakeDrug> takeDrugPage = takeDrugServiceImpl.findPageByOrderByCreateTimeDesc(takeDrug, pageNum, pageSize);
        List<TakeDrug> takeDrugs = takeDrugPage.getContent();
        for (TakeDrug td : takeDrugs) {
            int drugId = td.getDrugId();
            Drug drug = drugServiceImpl.getOne(drugId);
            td.setDrugInfo(drug.getName() + "*" + td.getDrugNum() + drug.getUnit());
        }
        return new PageImpl<>(takeDrugs, PageRequest.of(pageNum, pageSize), takeDrugPage.getTotalElements());
    }

    @GetMapping("selectTakeDrugPageByTimeAsc")//将查询结果按创建时间升序排序
    public Page<TakeDrug> selectTakeDrugPageByTimeAsc(String prescriptionNum, int pageNum, int pageSize) {
        TakeDrug takeDrug = new TakeDrug();
        takeDrug.setPrescriptionNum(prescriptionNum);
        Page<TakeDrug> takeDrugPage = takeDrugServiceImpl.findPageByOrderByCreateTimeAsc(takeDrug, pageNum, pageSize);
        List<TakeDrug> takeDrugs = takeDrugPage.getContent();
        for (TakeDrug td : takeDrugs) {
            int drugId = td.getDrugId();
            Drug drug = drugServiceImpl.getOne(drugId);
            td.setDrugInfo(drug.getName() + "*" + td.getDrugNum() + drug.getUnit());
        }
        return new PageImpl<>(takeDrugs, PageRequest.of(pageNum, pageSize), takeDrugPage.getTotalElements());
    }

    @GetMapping("selectBypNum")
    public List<TakeDrug> selectBypNum(String prescriptionNum) {
        return takeDrugServiceImpl.selectBypNum(prescriptionNum);
    }

    @GetMapping("selectDrugInfoByCardId")
    public TakeDrug selectDrugInfoByCardId(String cardId) {
        List<TakeDrug> takeDrugList = takeDrugServiceImpl.selectByCardId(cardId);
        if (takeDrugList.size() == 0) return null;
        StringBuilder drugInfo = new StringBuilder();
        for (TakeDrug td : takeDrugList) {
            OutPatient outPatient = outPatientService.selectOutPatient(cardId);
            td.setPayStatus(outPatient.getPayStatus());
            int drugId = td.getDrugId();
            Drug drug = drugServiceImpl.getOne(drugId);
            String drugName = drug.getName();
            td.setDrugName(drugName);
            drugInfo.append(drugName).append('*').append(td.getDrugNum()).append(" ");
        }
        TakeDrug takeDrug = takeDrugList.get(0);
        takeDrug.setDrugInfo(drugInfo.toString());
        return takeDrug;

    }

    @GetMapping("selectByCardId")
    public List<TakeDrug> selectByCardId(String cardId) {
        List<TakeDrug> takeDrugList = takeDrugServiceImpl.selectByCardId(cardId);
        if (takeDrugList.size() == 0) return null;
        for (TakeDrug td : takeDrugList) {
            Drug drug = drugServiceImpl.getOne(td.getDrugId());
            td.setDrugName(drug.getName());
            td.setSpecification(drug.getSpecification());
            td.setPrice(drug.getPrice());
        }
        return takeDrugList;
    }
}
