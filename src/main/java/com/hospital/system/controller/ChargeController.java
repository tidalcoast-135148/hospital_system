package com.hospital.system.controller;

import com.hospital.system.result.Result;
import com.hospital.system.service.OutPatientService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@RequestMapping("charge")
public class ChargeController {

    @Resource
    private OutPatientService outPatientService;

    @RequestMapping("updateOutPatient")
    public Result updateOutPatient(String cardId){
        return outPatientService.updateOutPatient(cardId)?Result.success("success"):Result.fail(1,"更新失败！");
    }
}
