package com.hospital.system.controller;

import com.hospital.system.components.TokenCache;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class captchaController {

    @RequestMapping("/captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CaptchaUtil.out(request, response);
        TokenCache.setRem("captcha",(String) request.getSession().getAttribute("captcha"));
        System.out.println("captcha:"+request.getSession().getAttribute("captcha"));
    }
}
