package com.hospital.system.controller;


import com.hospital.system.model.Examination;
import com.hospital.system.service.impl.ExaminationServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("examination")
public class ExaminationController {

    @Resource
    private ExaminationServiceImpl examinationServiceImpl;

    //插入体检信息
    @PostMapping("insert")
    public String insert(@RequestBody Examination examination){
        if(examinationServiceImpl.insert(examination))
            return "success";
        return "fail";
    }

    //插入或者更新体检信息
    @PostMapping("saveOrUpdate")
    public String saveOrUpdate(@RequestBody Examination examination){
        if(examinationServiceImpl.saveOrUpdate(examination) > 0)
            return "success";
        return "fail";
    }

    //根据卡号更新体检收费状态
    @PostMapping("updatePayStatus")
    public String updatePayStatus(@RequestBody Examination examination){
        if(examinationServiceImpl.updatePayStatus(examination.getCardId()) > 0)
            return "success";
        if(examinationServiceImpl.updatePayStatus(examination.getCardId()) < 0)
            return "done";
        return "fail";
    }

    //根据卡号搜索体检信息
    @GetMapping("selectByCardId")
    public Examination selectByCardId(String cardId){
        return examinationServiceImpl.selectByCardId(cardId);
    }


}
