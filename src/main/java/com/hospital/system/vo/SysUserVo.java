package com.hospital.system.vo;

public class SysUserVo {

    String id;
    String name;
    String passw;
    String createTime;
    String email;
    String phone;
    String workAddress;
    String sex;
    String birthday;
    String department;
    String departmentCode;
    Integer state;
    String roleName;


    public String getId() {
        return id;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SysUserVo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", passw='" + passw + '\'' +
                ", createTime='" + createTime + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", department='" + department + '\'' +
                ", departmentCode='" + departmentCode + '\'' +
                ", state=" + state +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
