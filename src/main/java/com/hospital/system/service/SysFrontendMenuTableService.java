package com.hospital.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hospital.system.entity.SysFrontendMenuTable;
import com.hospital.system.vo.SysFrontendVo;

import java.util.List;

public interface SysFrontendMenuTableService extends IService<SysFrontendMenuTable> {

    /**
     * 查所有的前端菜单（编辑使用）
     */
    List<SysFrontendVo> getAllMenuList();

//    List<SysFrontendVo> getChildren();
    /**
     * 根据登录账号，获得前端展现的菜单
     * 控制前端菜单的权限
     * @param username
     * @return
     */
    List<SysFrontendMenuTable> getMenusByUserName(String username);
}