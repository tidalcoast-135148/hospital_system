package com.hospital.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hospital.system.entity.SysBackendApiTable;
import com.hospital.system.vo.SysBackendApiVo;

import java.util.List;

public interface SysBackendApiTableService extends IService<SysBackendApiTable> {

    /**
     * 根据角色查询API接口URL
     * @param roles
     * @return
     */
    List<SysBackendApiTable> getApiUrlByRoles(String... roles);

    /**
     * 根据用户名称查询API接口URL
     * @param username
     * @return
     */
    List<SysBackendApiTable> getApiUrlByUserName(String username);

    /**
     * 查所有（编辑使用）
     */
    List<SysBackendApiVo> getAllApiList();
}