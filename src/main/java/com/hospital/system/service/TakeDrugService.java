package com.hospital.system.service;

import com.hospital.system.dao.TakeDrugRepository;
import com.hospital.system.model.TakeDrug;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TakeDrugService extends TakeDrugRepository {

    boolean insert(TakeDrug takeDrug);

    Page<TakeDrug> findPageByOrderByCreateTimeDesc (TakeDrug param, int pageNum, int pageSize);

    Page<TakeDrug> findPageByOrderByCreateTimeAsc (TakeDrug param, int pageNum, int pageSize);

}
