package com.hospital.system.service;

import com.hospital.system.dao.DoctorDao;
import com.hospital.system.model.Doctor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DoctorService {

    @Resource
    private DoctorDao doctorDao;

    public List<Doctor> selectDoctorsByDepartmentCode(String code){
        return doctorDao.selectDoctorsByDepartmentCode(code);
    }

    public Doctor selectDoctorById(String doctorCode){
        return doctorDao.selectDoctorById(doctorCode);
    }
}
