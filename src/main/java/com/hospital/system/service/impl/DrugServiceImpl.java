package com.hospital.system.service.impl;

import com.hospital.system.dao.DrugRepository;
import com.hospital.system.model.Drug;
import com.hospital.system.service.DrugService;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class DrugServiceImpl implements DrugService {

    @Resource
    private DrugRepository drugRepository;

    @Resource
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Page<Drug> findPage(Drug param, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize); //分页信息
        Specification<Drug> spec = new Specification<Drug>() { //查询条件构造
            @Override
            public Predicate toPredicate(Root<Drug> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<Predicate>();
                Path<String> name = root.get("name");
                if (param.getTypeCode() != null && param.getTypeCode() != 0)
                    list.add(criteriaBuilder.equal(root.get("typeCode").as(Integer.class), param.getTypeCode()));
                if (param.getTypeCode() != null && param.getEfficacyCode() != 0)
                    list.add(criteriaBuilder.equal(root.get("efficacyCode").as(Integer.class), param.getEfficacyCode()));
                list.add(criteriaBuilder.like(name, "%" + param.getName() + "%"));
                Predicate[] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            }
        };
        return drugRepository.findAll(spec, pageable);
    }

    @Override
    public List<Drug> searchDrug(String key) {
        Specification<Drug> spec = new Specification<Drug>() {
            @Override
            public Predicate toPredicate(Root<Drug> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Path<String> name = root.get("name");
                return cb.like(name, "%"+key+"%");
            }
        };
        return drugRepository.findAll(spec);
    }

    @Override
    public Boolean insert(Drug drug) {
        drug.setCreateTime(new Date());
        return drugRepository.save(drug) != null;
    }

    @Override
    public int updateNum(Integer id, Integer num) {
        return drugRepository.updateNum(id, num);
    }

    @Override
    public int deleteByTypeCode(Integer code) {
        return drugRepository.deleteByTypeCode(code);
    }

    @Override
    public int deleteByEfficacyCode(Integer code) {
        return drugRepository.deleteByEfficacyCode(code);
    }

    @Override
    public List<Drug> selectByTypeCode(Integer code) {

        return null;
    }

    @Override
    public int update(Integer typeCode, Integer efficacyCode, String manufacturer, String name, Integer num, String specification, String unit, Double price, Integer id) {
        return drugRepository.update(typeCode, efficacyCode, manufacturer, name, num, specification, unit, price, id);
    }


    @Override
    public List<Drug> findAll() {
        return drugRepository.findAll();
    }

    @Override
    public List<Drug> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<Drug> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public <S extends Drug> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Drug> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Drug> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Drug getOne(Integer id) {
        return drugRepository.getOne(id);
    }

    @Override
    public <S extends Drug> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Drug> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Optional<Drug> findOne(Specification<Drug> specification) {
        return Optional.empty();
    }

    @Override
    public List<Drug> findAll(Specification<Drug> specification) {
        return null;
    }

    @Override
    public Page<Drug> findAll(Specification<Drug> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Drug> findAll(Specification<Drug> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Drug> specification) {
        return 0;
    }

    @Override
    public Page<Drug> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Drug> S save(S s) {
        return null;
    }

    @Override
    public Optional<Drug> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer id) {
        drugRepository.deleteById(id);
    }

    @Override
    public void delete(Drug drug) {

    }

    @Override
    public void deleteAll(Iterable<? extends Drug> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Drug> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Drug> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Drug> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Drug> boolean exists(Example<S> example) {
        return false;
    }
}

