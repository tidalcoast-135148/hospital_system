package com.hospital.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospital.system.dao.SysFrontendMenuTableDao;
import com.hospital.system.entity.SysFrontendMenuTable;
import com.hospital.system.service.SysFrontendMenuTableService;
import com.hospital.system.vo.SysFrontendVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("sysFrontendMenuTableService")
public class SysFrontendMenuTableServiceImpl extends ServiceImpl<SysFrontendMenuTableDao, SysFrontendMenuTable> implements SysFrontendMenuTableService {

    /**
     * 查所有的前端菜单（编辑使用）
     */
    @Override
   public List<SysFrontendVo> getAllMenuList(){
       return this.baseMapper.getAllMenuList();
    }

//    @Override
//    public List<SysFrontendVo> getChildren() {
//        return this.baseMapper.getChildren();
//    }

    /**
     * 根据登录账号，获得前端展现的菜单
     * 控制前端菜单的权限
     * @param username
     * @return
     */
    @Override
    public List<SysFrontendMenuTable> getMenusByUserName(String username){
        return this.baseMapper.getMenusByUserName(username);
    }
}