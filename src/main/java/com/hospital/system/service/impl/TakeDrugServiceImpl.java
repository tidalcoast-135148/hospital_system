package com.hospital.system.service.impl;

import com.hospital.system.dao.TakeDrugRepository;
import com.hospital.system.model.Drug;
import com.hospital.system.model.TakeDrug;
import com.hospital.system.service.TakeDrugService;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TakeDrugServiceImpl implements TakeDrugService {

    @Resource
    private TakeDrugRepository takeDrugRepository;

    @Override
    public List<TakeDrug> findAll() {
        return takeDrugRepository.findAll();
    }

    @Override
    public List<TakeDrug> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<TakeDrug> selectBypNum(String pNum) {
        return takeDrugRepository.selectBypNum(pNum);
    }

    @Override
    public List<TakeDrug> selectByCardId(String cardId) {
        return takeDrugRepository.selectByCardId(cardId);
    }

    @Override
    public Page<TakeDrug> findPageByOrderByCreateTimeDesc(TakeDrug param,int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by("createTime").descending());//按时间降序分页
        Specification<TakeDrug> spec = new Specification<TakeDrug>() {
            @Override
            public Predicate toPredicate(Root<TakeDrug> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Path<String> pNum = root.get("prescriptionNum");
                if("".equals(param.getPrescriptionNum()) || param.getPrescriptionNum() == null){
                    return cb.like(pNum.as(String.class), "%%");
                }
                return cb.equal(pNum.as(String.class), param.getPrescriptionNum());
            }
        };
        return takeDrugRepository.findAll(spec, pageable);
    }

    @Override
    public Page<TakeDrug> findPageByOrderByCreateTimeAsc(TakeDrug param,int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by("createTime").ascending());//按时间升序分页
        Specification<TakeDrug> spec = new Specification<TakeDrug>() {
            @Override
            public Predicate toPredicate(Root<TakeDrug> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Path<String> pNum = root.get("prescriptionNum");
                if("".equals(param.getPrescriptionNum()) || param.getPrescriptionNum() == null){
                    return cb.like(pNum.as(String.class), "%%");
                }
                return cb.equal(pNum.as(String.class), param.getPrescriptionNum());
            }
        };
        return takeDrugRepository.findAll(spec, pageable);

    }

    @Override
    public List<TakeDrug> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public <S extends TakeDrug> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends TakeDrug> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<TakeDrug> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public TakeDrug getOne(Integer id) {
        return takeDrugRepository.getOne(id);
    }

    @Override
    public <S extends TakeDrug> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends TakeDrug> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Page<TakeDrug> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TakeDrug> S save(S s) {
        return null;
    }

    @Override
    public boolean insert(TakeDrug takeDrug) {
        takeDrug.setCreateTime(new Date());
        takeDrug.setTakeStatus(0);
        return takeDrugRepository.save(takeDrug) != null;
    }

    @Override
    public int updateBypNum(String pNum, Date takeTime) {
        List<TakeDrug> takeDrugs = selectBypNum(pNum);
        int num = takeDrugs.size();
        for(TakeDrug td : takeDrugs){
            if(td.getTakeStatus() == 1){
                num--;
            }
        }
        if(num == takeDrugs.size() || num > 0)
            return takeDrugRepository.updateBypNum(pNum, takeTime);
        return -1;
    }

    @Override
    public Optional<TakeDrug> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(TakeDrug takeDrug) {

    }

    @Override
    public void deleteAll(Iterable<? extends TakeDrug> iterable) {

    }

    @Override
    public void deleteAll() {
        takeDrugRepository.deleteAll();
    }

    @Override
    public <S extends TakeDrug> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends TakeDrug> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends TakeDrug> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends TakeDrug> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public Optional<TakeDrug> findOne(Specification<TakeDrug> specification) {
        return Optional.empty();
    }

    @Override
    public List<TakeDrug> findAll(Specification<TakeDrug> specification) {
        return null;
    }

    @Override
    public Page<TakeDrug> findAll(Specification<TakeDrug> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<TakeDrug> findAll(Specification<TakeDrug> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<TakeDrug> specification) {
        return 0;
    }
}
