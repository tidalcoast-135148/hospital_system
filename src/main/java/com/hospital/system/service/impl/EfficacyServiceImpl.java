package com.hospital.system.service.impl;

import com.hospital.system.dao.DrugRepository;
import com.hospital.system.dao.EfficacyRepository;
import com.hospital.system.model.Efficacy;
import com.hospital.system.service.EfficacyService;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

@Service
public class EfficacyServiceImpl implements EfficacyService {

    @Resource
    private EfficacyRepository efficacyRepository;

    @Resource
    private DrugRepository drugRepository;

    @Resource
    private EntityManagerFactory entityManagerFactory;


    @Override
    public int updateById(Integer id, String name) {
        return efficacyRepository.updateById(id, name);
    }

    @Override
    public Boolean insert(String name) {
        EntityManager em = entityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);//查询的结果类型
        Root<Efficacy> root = cq.from(Efficacy.class);
        cq.select(cb.greatest((Path)root.get("code")));//查询最大的code值
        TypedQuery<Integer> typedQuery = em.createQuery(cq);
        Integer maxCode = typedQuery.getSingleResult();
        if(maxCode == null)
            maxCode = 0;
        Efficacy efficacy = new Efficacy();
        efficacy.setCode(maxCode + 1);
        efficacy.setName(name);
        return efficacyRepository.save(efficacy) != null;
    }

    @Override
    public Efficacy findByName(String name) {
        return efficacyRepository.findByName(name);
    }



    @Override
    public List<Efficacy> findAll(Specification<Efficacy> specification) {
        return null;
    }

    @Override
    public List<Efficacy> findAll() {
        return efficacyRepository.findAll();
    }

    @Override
    public List<Efficacy> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<Efficacy> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public <S extends Efficacy> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Efficacy> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Efficacy> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Efficacy getOne(Integer id) {
        return efficacyRepository.getOne(id);
    }

    @Override
    public <S extends Efficacy> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Efficacy> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Optional<Efficacy> findOne(Specification<Efficacy> specification) {
        return Optional.empty();
    }

    @Override
    public Page<Efficacy> findAll(Specification<Efficacy> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Efficacy> findAll(Specification<Efficacy> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Efficacy> specification) {
        return 0;
    }

    @Override
    public Page<Efficacy> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Efficacy> S save(S s) {
        return efficacyRepository.save(s);
    }

    @Override
    public Optional<Efficacy> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {
        efficacyRepository.deleteById(integer);
    }

    @Override
    public int delete(Integer id) {

        return 0;
    }

    @Override
    public List<Efficacy> gainByCode(Integer code) {

        return efficacyRepository.findAll(new Specification<Efficacy>() {
            @Override
            public Predicate toPredicate(Root<Efficacy> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Path<Integer> efficacy = root.get("code");
                return cb.equal(efficacy, code);
            }
        });
    }

    @Override
    public void delete(Efficacy efficacy) {
        efficacyRepository.delete(efficacy);
    }

    @Override
    public int deleteByCode(Integer code) {
        int result1 = drugRepository.deleteByEfficacyCode(code);
        int result2 =  efficacyRepository.deleteByCode(code);
        if(result1 > 0 || result2 > 0)
            return 1;
        return -1;
    }

    @Override
    public void deleteAll(Iterable<? extends Efficacy> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Efficacy> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Efficacy> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Efficacy> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Efficacy> boolean exists(Example<S> example) {
        return false;
    }
}
