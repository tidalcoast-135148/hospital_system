package com.hospital.system.service.impl;

import com.hospital.system.dao.ExaminationRepository;
import com.hospital.system.model.Examination;
import com.hospital.system.service.ExaminationService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ExaminationServiceImpl implements ExaminationService {

    @Resource
    private ExaminationRepository examinationRepository;


    @Override
    public boolean insert(Examination examination) {
        examination.setCreateTime(new Date());
        return examinationRepository.save(examination) != null;
    }

    @Override
    public int updatePayStatus(String cardId) {
        Examination e = selectByCardId(cardId);
        if(e == null)
            return 0;
        if(e.getPayStatus() == 1){
            return -1;
        }
        return examinationRepository.updatePayStatus(cardId);
    }

    @Override
    public int update(String cardId, Integer bloodPressure, Double bodyTemperature, Double cost, Integer heartRate, Integer pulse) {
        return examinationRepository.update(cardId, bloodPressure, bodyTemperature, cost,
                heartRate,pulse);
    }

    @Override
    public int saveOrUpdate(Examination examination) {
        Examination e = selectByCardId(examination.getCardId());
        if(e != null){
            int result = update(examination.getCardId(), examination.getBloodPressure(), examination.getBodyTemperature(),
                    examination.getCost(), examination.getHeartRate(), examination.getPulse());
            if(result > 0){
                return 1;
            }
        }else {
             if(insert(examination))
                 return 1;
        }
        return 0;
    }

    @Override
    public Examination selectByCardId(String cardId) {
        return examinationRepository.selectByCardId(cardId);
    }

    @Override
    public List<Examination> findAll() {
        return null;
    }

    @Override
    public List<Examination> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<Examination> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public <S extends Examination> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Examination> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Examination> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Examination getOne(Integer integer) {
        return null;
    }

    @Override
    public <S extends Examination> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Examination> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Optional<Examination> findOne(Specification<Examination> specification) {
        return Optional.empty();
    }

    @Override
    public List<Examination> findAll(Specification<Examination> specification) {
        return null;
    }

    @Override
    public Page<Examination> findAll(Specification<Examination> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<Examination> findAll(Specification<Examination> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<Examination> specification) {
        return 0;
    }

    @Override
    public Page<Examination> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Examination> S save(S s) {
        return null;
    }

    @Override
    public Optional<Examination> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Examination examination) {

    }

    @Override
    public void deleteAll(Iterable<? extends Examination> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Examination> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Examination> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Examination> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Examination> boolean exists(Example<S> example) {
        return false;
    }
}
