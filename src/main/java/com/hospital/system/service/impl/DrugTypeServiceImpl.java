package com.hospital.system.service.impl;

import com.hospital.system.dao.DrugRepository;
import com.hospital.system.dao.DrugTypeRepository;
import com.hospital.system.model.DrugType;
import com.hospital.system.service.DrugTypeService;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

@Service
public class DrugTypeServiceImpl implements DrugTypeService {

    @Resource
    private DrugTypeRepository drugTypeRepository;

    @Resource
    private DrugRepository drugRepository;

    @Resource
    private EntityManagerFactory entityManagerFactory;

    @Override
    public boolean insert(String name) {
        EntityManager em = entityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
        Root<DrugType> root = cq.from(DrugType.class);
        cq.select(cb.greatest((Path)root.get("code")));//查询最大的code值
        Integer maxCode;
        TypedQuery<Integer> queryResult = em.createQuery(cq);//获取结果
        maxCode = queryResult.getSingleResult();
        if(maxCode == null)
            maxCode = 0;
        DrugType drugType = new DrugType();
        drugType.setName(name);
        drugType.setCode(maxCode + 1);
        return drugTypeRepository.save(drugType) != null;
    }

    @Override
    public int updateById(Integer id, String name) {
        return drugTypeRepository.updateById(id, name);
    }

    @Override
    public List<DrugType> findAll() {
        return drugTypeRepository.findAll();
    }

    @Override
    public List<DrugType> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<DrugType> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public <S extends DrugType> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends DrugType> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<DrugType> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public DrugType getOne(Integer id) {
        return drugTypeRepository.getOne(id);
    }

    @Override
    public <S extends DrugType> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends DrugType> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Optional<DrugType> findOne(Specification<DrugType> specification) {
        return Optional.empty();
    }

    @Override
    public List<DrugType> findAll(Specification<DrugType> specification) {
        return null;
    }

    @Override
    public Page<DrugType> findAll(Specification<DrugType> specification, Pageable pageable) {
        return null;
    }

    @Override
    public List<DrugType> findAll(Specification<DrugType> specification, Sort sort) {
        return null;
    }

    @Override
    public long count(Specification<DrugType> specification) {
        return 0;
    }

    @Override
    public Page<DrugType> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DrugType> S save(S s) {
        return null;
    }

    @Override
    public Optional<DrugType> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer id) {
        drugTypeRepository.deleteById(id);
    }

    @Override
    public int deleteByCode(Integer code) {
        int result = drugRepository.deleteByTypeCode(code);
        int result2 = drugTypeRepository.deleteByCode(code);
        if(result > 0 || result2 > 0)
            return 1;
        return -1;
    }

    @Override
    public int delete(Integer id) {

        return 0;

    }

    @Override
    public List<DrugType> gainByCode(Integer code) {
        return drugTypeRepository.findAll(new Specification<DrugType>() {
            @Override
            public Predicate toPredicate(Root<DrugType> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Path<Integer> typeCode = root.get("code");
                return cb.equal(typeCode, code);
            }
        });
    }

    @Override
    public void delete(DrugType drugType) {
        drugTypeRepository.delete(drugType);
    }

    @Override
    public void deleteAll(Iterable<? extends DrugType> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends DrugType> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends DrugType> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends DrugType> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends DrugType> boolean exists(Example<S> example) {
        return false;
    }
}
