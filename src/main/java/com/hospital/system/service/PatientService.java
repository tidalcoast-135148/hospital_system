package com.hospital.system.service;

import com.hospital.system.dao.PatientDao;
import com.hospital.system.model.Patient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PatientService {

    @Resource
    private PatientDao patientDao;

    public boolean insertPatient(Patient patient){
        return patientDao.insertPatient(patient)>0?true:false;
    }

    public Patient selectPatient(String cardId){
        return patientDao.selectPatient(cardId);
    }

    public boolean updatePatient(String cardId,String personalHistory,String familyHistory){
        int t = patientDao.updatePatient(cardId,personalHistory,familyHistory);
        return t>0?true:false;
    }
}
