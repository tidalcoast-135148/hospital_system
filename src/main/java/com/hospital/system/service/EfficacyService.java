package com.hospital.system.service;

import com.hospital.system.dao.EfficacyRepository;
import com.hospital.system.model.Efficacy;

import java.util.List;


public interface EfficacyService extends EfficacyRepository{

    Boolean insert(String name);

    int updateById(Integer id, String name);

    void deleteById(Integer id);

    int delete(Integer id);

    List<Efficacy> gainByCode(Integer code);
}
