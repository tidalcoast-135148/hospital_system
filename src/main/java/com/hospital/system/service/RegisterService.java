package com.hospital.system.service;

import com.hospital.system.dao.RegisterDao;
import com.hospital.system.model.Register;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RegisterService {

    @Resource
    private RegisterDao registerDao;

    public boolean insertRegister(Register register){
        return registerDao.insertRegister(register)>0?true:false;
    }

    public List<Register> selectRegister(){
        return registerDao.selectRegister();
    }

    public Register selectOneRegister(String cardId){
        return registerDao.selectOneRegister(cardId);
    }

    public List<Register> selectRegisterForPage(int page){
        page = (page-1)*10;
        return registerDao.selectRegisterForPage(page);
    }

    public List<Register> selectRegisterByDepartmentCode(String department){
        return registerDao.selectRegisterByDepartmentCode(department);
    }
}
