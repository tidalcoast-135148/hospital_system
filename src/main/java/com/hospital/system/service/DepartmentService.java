package com.hospital.system.service;

import com.hospital.system.dao.DepartmentDao;
import com.hospital.system.model.Department;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DepartmentService {

    @Resource
    private DepartmentDao departmentDao;

    public Department selectDepartmentByName(String name){
        return departmentDao.selectDepartmentByName(name);
    }

    public Department selectDepartmentByCode(String departmentCode){
        return departmentDao.selectDepartmentByCode(departmentCode);
    }

    public List<Department> selectAll(){
        return departmentDao.selectAll();
    }
}
