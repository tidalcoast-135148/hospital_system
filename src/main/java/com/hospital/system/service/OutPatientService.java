package com.hospital.system.service;

import com.hospital.system.dao.OutPatientDao;
import com.hospital.system.model.OutPatient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OutPatientService {

    @Resource
    private OutPatientDao outPatientDao;

    public boolean insertOutPatient(OutPatient outPatient){
        return outPatientDao.insertOutPatient(outPatient)>0?true:false;
    }

    public OutPatient selectOutPatient(String cardId){
        return outPatientDao.selectOutPatient(cardId);
    }

    public boolean updateOutPatient(String cardId){
        return outPatientDao.updateOutPatient(cardId)>0?true:false;
    }
}
