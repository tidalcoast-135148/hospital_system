package com.hospital.system.service;

import com.hospital.system.dao.DrugRepository;
import com.hospital.system.model.Drug;
import org.springframework.data.domain.Page;

import java.util.List;


public interface DrugService extends DrugRepository {

    Page<Drug> findPage(Drug param, int pageNum, int pageSize);

    Boolean insert(Drug drug);

    List<Drug> selectByTypeCode(Integer code);

    List<Drug> searchDrug(String key);

}
