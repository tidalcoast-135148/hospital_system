package com.hospital.system.service;

import com.hospital.system.dao.ExaminationRepository;
import com.hospital.system.model.Examination;

public interface ExaminationService extends ExaminationRepository {

    boolean insert(Examination examination);

    int saveOrUpdate(Examination examination);
}
