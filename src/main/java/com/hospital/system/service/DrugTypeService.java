package com.hospital.system.service;


import com.hospital.system.dao.DrugTypeRepository;
import com.hospital.system.model.DrugType;

import java.util.List;

public interface DrugTypeService extends DrugTypeRepository {

    boolean insert(String name);

    int delete(Integer id);

    List<DrugType> gainByCode(Integer code);
}
