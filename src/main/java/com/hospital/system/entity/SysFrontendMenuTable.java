package com.hospital.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")

public class SysFrontendMenuTable extends Model<SysFrontendMenuTable> {

    @TableId(type = IdType.UUID)
    private String frontendMenuId;

    private String frontendMenuName;

    private String frontendMenuUrl;

    private String pid;

    private Integer frontendMenuSort;

    private String description;

    private String icon;


    @TableField(exist = false)
    private List<SysFrontendMenuTable> children = new ArrayList<>();

    public List<SysFrontendMenuTable> getChildren() {
        return children;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setChildren(List<SysFrontendMenuTable> children) {
        this.children = children;
    }

    public String getFrontendMenuId() {
        return frontendMenuId;
    }

    public void setFrontendMenuId(String frontendMenuId) {
        this.frontendMenuId = frontendMenuId;
    }

    public String getFrontendMenuName() {
        return frontendMenuName;
    }

    public void setFrontendMenuName(String frontendMenuName) {
        this.frontendMenuName = frontendMenuName;
    }

    public String getFrontendMenuUrl() {
        return frontendMenuUrl;
    }

    public void setFrontendMenuUrl(String frontendMenuUrl) {
        this.frontendMenuUrl = frontendMenuUrl;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getFrontendMenuSort() {
        return frontendMenuSort;
    }

    public void setFrontendMenuSort(Integer frontendMenuSort) {
        this.frontendMenuSort = frontendMenuSort;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SysFrontendMenuTable{" +
                "frontendMenuId='" + frontendMenuId + '\'' +
                ", frontendMenuName='" + frontendMenuName + '\'' +
                ", frontendMenuUrl='" + frontendMenuUrl + '\'' +
                ", pid='" + pid + '\'' +
                ", frontendMenuSort=" + frontendMenuSort +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", children=" + children +
                '}';
    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.frontendMenuId;
    }
}