package com.hospital.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;


@TableName("t_user")
public class SysUserEntity {

    @TableId(type = IdType.UUID)
    String userId;

    String userName;

    String passWord;

    Date createTime;

    String email;

    String phone;

    String workAddress;

    String sex;

    String plainPassword;

    Date birthday;

    String departmentCode;

    Integer state;

    public SysUserEntity() {
    }

    public SysUserEntity(String userId, String userName, String passWord, Date createTime, String email, String phone, String workAddress, String sex, String plainPassword, Date birthday, String departmentCode, Integer state) {
        this.userId = userId;
        this.userName = userName;
        this.passWord = passWord;
        this.createTime = createTime;
        this.email = email;
        this.phone = phone;
        this.workAddress = workAddress;
        this.sex = sex;
        this.plainPassword = plainPassword;
        this.birthday = birthday;
        this.departmentCode = departmentCode;
        this.state = state;
    }

    @Override
    public String toString() {
        return "SysUserEntity{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", createTime=" + createTime +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", sex='" + sex + '\'' +
                ", plainPassword='" + plainPassword + '\'' +
                ", birthday=" + birthday +
                ", departmentCode='" + departmentCode + '\'' +
                ", state=" + state +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(String plainPassword) {
        this.plainPassword = plainPassword;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
